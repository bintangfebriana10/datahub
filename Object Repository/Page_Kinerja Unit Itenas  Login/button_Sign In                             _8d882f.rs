<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In                             _8d882f</name>
   <tag></tag>
   <elementGuidId>67c681a8-6e4e-4506-a3a7-251df156a6cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='kt_sign_in_submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#kt_sign_in_submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4314ab2d-3185-4a3c-b90b-2a8e266cf96a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>50e17623-1a63-4c36-9d49-3f13721a0371</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kt_sign_in_submit</value>
      <webElementGuid>965a82bc-6a69-4551-b01b-0ee83ba1a3c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>3aaecdd6-4869-4fe7-8655-8ad1a1d22443</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                      
                      Sign In
                      
                      
                      Please wait... 
                      
                    </value>
      <webElementGuid>84c5fc9f-cfe9-4bc7-aae0-20bb2604a9ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kt_sign_in_submit&quot;)</value>
      <webElementGuid>064892dc-c356-4836-a817-aca3cdfd90fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='kt_sign_in_submit']</value>
      <webElementGuid>f1481c01-98ca-424e-92c1-7713d6805e7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='kt_sign_in_form']/div[5]/button</value>
      <webElementGuid>e6d3bf8b-7c76-40ae-9d17-b6c62abb3d06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>3af9c930-de1d-49b4-abdf-1b779a616854</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::button[1]</value>
      <webElementGuid>c42301fa-0282-4362-bb1a-194667825e0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/button</value>
      <webElementGuid>f52a59f0-06c6-430c-88e6-cc823656ceec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'kt_sign_in_submit' and (text() = '
                      
                      Sign In
                      
                      
                      Please wait... 
                      
                    ' or . = '
                      
                      Sign In
                      
                      
                      Please wait... 
                      
                    ')]</value>
      <webElementGuid>2918c574-f159-412f-9ba8-3dab8db45041</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
