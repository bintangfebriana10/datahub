import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div/div/div[3]/span/span[2]'))

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div/div/div[3]/div/div/a[4]/span[2]'))

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'isi_index\']'))

WebUI.selectOptionByValue(findTestObject('selectunit'), unit, false)

WebUI.selectOptionByValue(findTestObject('selectperiode'), '5', false)

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div/div[2]/div[2]/div[1]/div/div/div/div/div[2]/div[2]/div[2]/div/div/div[1]/table/tbody/tr[14]/td[6]/div/a'))

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id="kt_app_content_container"]/div/div/div[1]/div[1]/div/a'))

WebUI.setText(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'isiIndi-69\']'), nim)

WebUI.setText(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'isiIndi-70\']'), nama)

WebUI.setText(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'isiIndi-71\']'), program)

WebUI.setText(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id=\'isiIndi-72\']'), date)

WebUI.selectOptionByValue(new TestObject().addProperty('xpath', ConditionType.EQUALS, '//*[@id="UnitId"]'), unit, false)

WebUI.delay(2)

WebUI.click(new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[1]/div/div[2]/div[2]/div[1]/div/div/div/div/div[2]/div/div/form/div[2]/button[2]'))

WebUI.delay(2)

WebUI.verifyElementPresent(findTestObject('verifyok'), 0)

